$(document).ready(function() {
    // fullpage
    $('#wrapper').fullpage({
        menu: '.fullpage-menu',
        anchors: ['firstSection', 'secondSection', 'thirdSection', 'fourthSection', 'fifthSection', 'sixSection'],
        responsive: 1200,
        easingcss3: 'cubic-bezier(0.175, 0.885, 0.320, 1.275)'
    });

    // END:fullpage
    // section0
    $('#section0 .slider-list').slick({
        arrows: false,
        dots: true,
        autoplay: true,
        autoplaySpeed: 10000,
        speed: 1000,
    });
    // END:section0
    // section2
    $('#section2 .slider-list').slick({
        arrows: false,
        dots: true,
        slidesToShow: 4,
        slidesToSсroll: 1,
        autoplay: true,
        autoplaySpeed: 10000,
        speed: 1000,
        variableWidth: true
    });
    // END:section2
    // section4
    $('#section4 .slider-nav').slick({
        arrows: false,
        dots: true,
        slidesToShow: 1,
        slidesToSсroll: 1,
        autoplay: true,
        autoplaySpeed: 10000,
        speed: 1000,
        asNavFor: '#section4 .slider-for',
    });
    $('#section4 .slider-for').slick({
        arrows: false,
        slidesToShow: 1,
        slidesToSсroll: 1,
        autoplay: true,
        autoplaySpeed: 10000,
        speed: 1000,
        asNavFor: '#section4 .slider-nav'
    });
    // END:section4
});
