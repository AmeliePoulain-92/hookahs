var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var prettify = require('gulp-jsbeautifier');

gulp.task('scss', function() {
	return gulp.src('app/scss/*.scss')
	.pipe(sass().on('error', sass.logError))
	.pipe(autoprefixer({
		browsers: ['last 5 versions', '>0%', 'ie 7'],
		cascade: false
	}))
	.pipe(prettify())
	.pipe(gulp.dest('app/css'));
});

gulp.task('html', function(){
	return gulp.src('app/*.html')
	.pipe(prettify())
	.pipe(gulp.dest('app/'));
});

gulp.task('watch', function(){
	gulp.watch('app/scss/assets/**/*.scss', ['scss']);
	gulp.watch('app/scss/**/*.scss', ['scss']);
	gulp.watch('app/js/**/*.js', ['default']);
	gulp.watch('gulpfile.js', ['default']);
});

gulp.task('default', ['scss', 'watch']);